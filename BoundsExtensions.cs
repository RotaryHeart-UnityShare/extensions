﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class BoundsExtensions
    {

        /// <summary>
        /// Returns a random point inside this bounds
        /// </summary>
        public static Vector3 GetRandomPointInside(this Bounds bounds)
        {
            return bounds.center +
                   Vector3.right * Random.Range(-bounds.extents.x, bounds.extents.x) +
                   Vector3.up * Random.Range(-bounds.extents.y, bounds.extents.y) +
                   Vector3.forward * Random.Range(-bounds.extents.z, bounds.extents.z);
        }

        /// <summary>
        /// Returns the encapsulated bounds of all the children objects that are on any of the specified layers in <paramref name="layerMask"/> and don't have any of the components <paramref name="ignoreWith"/> 
        /// </summary>
        /// <param name="layerMask">Layer mask to check, pass -1 to include all</param>
        /// <param name="includeInactive">Should inactive objects be included</param>
        /// <param name="ignoreWith">Components to ignore, pass null to include all</param>
        public static Bounds GetBounds(this GameObject go, int layerMask = -1, bool includeInactive = false, params System.Type[] ignoreWith)
        {
            Renderer[] renders = go.GetComponentsInChildren<Renderer>(includeInactive);
            Bounds bounds = new Bounds();
            bool skip;
            bool first = true;

            foreach (Renderer renderer in renders)
            {
                if (layerMask != -1 && ((layerMask & (1 << renderer.gameObject.layer)) != 0))
                    continue;

                skip = false;

                if (ignoreWith != null)
                {
                    foreach (System.Type type in ignoreWith)
                    {
                        if (renderer.GetComponent(type))
                        {
                            skip = true;
                            break;
                        }
                    }
                }

                if (skip)
                    continue;

                if (first)
                    bounds = renderer.bounds;
                else
                    bounds.Encapsulate(renderer.bounds);

                first = false;
            }

            return bounds;
        }

        /// <summary>
        /// Returns the encapsulated mesh bound (ignores rotations) of all the children meshes on this GameObject
        /// </summary>
        /// <param name="includeInactive">Should inactive objects be included</param>
        public static Bounds GetMeshBounds(this GameObject go, bool includeInactive = false)
        {
            Bounds bounds = new Bounds();
            bool first = true;

            MeshFilter[] rends = go.GetComponentsInChildren<MeshFilter>(includeInactive);
            SkinnedMeshRenderer[] rends2 = go.GetComponentsInChildren<SkinnedMeshRenderer>(includeInactive);

            if (rends != null)
            {
                first = false;
                bounds = rends[0].sharedMesh.bounds;

                foreach (MeshFilter rend in rends)
                {
                    bounds.Encapsulate(rend.sharedMesh.bounds);
                }
            }

            if (rends2 != null)
            {
                if (first)
                    bounds = rends2[0].sharedMesh.bounds;

                foreach (SkinnedMeshRenderer rend in rends2)
                {
                    bounds.Encapsulate(rend.sharedMesh.bounds);
                }
            }

            return bounds;
        }

        /// <summary>
        /// Returns the encapsulated bounds of all the children objects that are on any of the specified layers in <paramref name="layerMask"/> and don't have any of the components <paramref name="ignoreWith"/> 
        /// </summary>
        /// <param name="layerMask">Layer mask to check, pass -1 to include all</param>
        /// <param name="includeInactive">Should inactive objects be included</param>
        /// <param name="ignoreWith">Components to ignore, pass null to include all</param>
        public static Bounds GetBounds(this Transform trs, int layerMask = -1, bool includeInactive = false, params System.Type[] ignoreWith)
        {
            return GetBounds(trs.gameObject, layerMask, includeInactive, ignoreWith);
        }

    }
}
