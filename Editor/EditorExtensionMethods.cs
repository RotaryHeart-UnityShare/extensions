using UnityEngine;
using UnityEditor;
using System.Reflection;

namespace RotaryHeart.Lib.Extensions
{
    /// <summary>
    /// Needs to be inside an editor folder
    /// </summary>
    public static class EditorExtensionMethods
    {
        //Used to draw rects with color
        static readonly Texture2D backgroundTexture = Texture2D.whiteTexture;
        static readonly GUIStyle textureStyle = new GUIStyle { normal = new GUIStyleState { background = backgroundTexture } };
        static PropertyInfo inspectorModeInfo = typeof(SerializedObject).GetProperty("inspectorMode", BindingFlags.NonPublic | BindingFlags.Instance);

        /// <summary>
        /// Draws a rect with a solid color
        /// </summary>
        /// <param name="position">Position to draw the rect</param>
        /// <param name="color">Color to draw the rect</param>
        /// <param name="content">Content, if any</param>
        public static void DrawRect(Rect position, Color color, GUIContent content = null)
        {
            var backgroundColor = GUI.backgroundColor;
            GUI.backgroundColor = color;
            GUI.Box(position, content ?? GUIContent.none, textureStyle);
            GUI.backgroundColor = backgroundColor;
        }

        /// <summary>
        /// Returns the FileId of an Object
        /// </summary>
        /// <param name="obj">Object to check</param>
        /// <returns>FileId value</returns>
        public static long GetFileId(Object obj)
        {
            if (obj == null)
                return -1;

            SerializedObject serializedObject = new SerializedObject(obj);
            inspectorModeInfo.SetValue(serializedObject, InspectorMode.Debug, null);

            SerializedProperty localIdProp = serializedObject.FindProperty("m_LocalIdentfierInFile");   //note the misspelling!

            return localIdProp.longValue;
        }

        /// <summary>
        /// Returns the proper property drawer, including custom ones, for this property
        /// </summary>
        /// <param name="property">The property to get the drawer from</param>
        /// <param name="fieldInfo">Field info to return custom property drawer</param>
        public static PropertyDrawer GetPropertyDrawer(this SerializedProperty property, FieldInfo fieldInfo)
        {
            // Getting the field type this way assumes that the property instance is not a managed reference (with a SerializeReference attribute); if it was, it should be retrieved in a different way:
            System.Type fieldType = fieldInfo.FieldType;

            System.Type propertyDrawerType = (System.Type)System.Type.GetType("UnityEditor.ScriptAttributeUtility,UnityEditor")
                ?.GetMethod("GetDrawerTypeForPropertyAndType", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public)
                ?.Invoke(null, new object[] { property, fieldType });

            if (propertyDrawerType == null)
            {
                return null;
            }

            PropertyDrawer propertyDrawer = null;
            if (typeof(PropertyDrawer).IsAssignableFrom(propertyDrawerType))
            {
                propertyDrawer = (PropertyDrawer)System.Activator.CreateInstance(propertyDrawerType);
            }

            if (propertyDrawer != null)
            {
                FieldInfo fi = typeof(PropertyDrawer).GetField("m_FieldInfo", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

                if (fi == null)
                {
                    Debug.LogError("Error trying to find PropertyDrawer.m_FieldInfo");
                    return null;
                }

                fi.SetValue(propertyDrawer, fieldInfo);

                return propertyDrawer;
            }

            return null;
        }

        /// <summary>
        /// Searches for an asset using AssetDatabase and returns the full path to the file
        /// </summary>
        /// <param name="pathEnd">The end path to use for finding the asset</param>
        public static string FindAsset(string pathEnd)
        {
            string[] guids = AssetDatabase.FindAssets(System.IO.Path.GetFileNameWithoutExtension(pathEnd));
            string assetPath = null;

            foreach (string guid in guids)
            {
                if (AssetDatabase.GUIDToAssetPath(guid).EndsWith(pathEnd))
                {
                    assetPath = AssetDatabase.GUIDToAssetPath(guid);
                    break;
                }
            }

            if (string.IsNullOrEmpty(assetPath))
            {
                Debug.LogError($"Couldn't find {System.IO.Path.GetFileName(pathEnd)} file");
            }
            return assetPath;
        }
    }
}