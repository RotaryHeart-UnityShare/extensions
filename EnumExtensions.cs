﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

namespace RotaryHeart.Lib.Extensions
{
    public static class EnumExtensions
    {

        /// <summary>
        /// Returns a random value from an enum
        /// </summary>
        public static T GetRandomValue<T>() where T : System.Enum
        {
            T[] values = GetEnumValues<T>();
            return values[UnityEngine.Random.Range(0, values.Length)];
        }

        /// <summary>
        /// Returns all the values from an enum
        /// </summary>
        public static T[] GetEnumValues<T>() where T : System.Enum
        {
            return (T[])System.Enum.GetValues(typeof(T));
        }

        /// <summary>
        /// Function used to check if this enum mask contains a specific enum value
        /// </summary>
        /// <param name="checkValue">Value to check on mask</param>
        public static bool Contains<T>(this T thisType, T checkValue) where T : System.Enum
        {
            return thisType.Contains((int)(System.IConvertible)checkValue);
        }

        /// <summary>
        /// Function used to check if this enum mask contains a specific enum value
        /// </summary>
        /// <param name="checkValue">Value to check on mask</param>
        public static bool Contains<T>(this T thisType, int checkValue) where T : System.Enum
        {
            return ((int)(System.IConvertible)thisType & checkValue) != 0;
        }

        /// <summary>
        /// Function used to convert an enum to another enum
        /// </summary>
        /// <returns>The converted value</returns>
        public static T Convert<T>(this System.IConvertible enumVal) where T : System.Enum
        {
            if (System.Enum.TryParse(typeof(T), enumVal.ToString(), out object result))
            {
                return (T)result;
            }
            return default;
        }
    }
}