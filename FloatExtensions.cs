﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Runtime.CompilerServices;
using UnityEngine;
#if Mathematics
using Unity.Mathematics;
#endif

namespace RotaryHeart.Lib.Extensions
{
    public static class FloatExtensions
    {

        /// <summary>
        /// Ensures the float value is a multiple of <paramref name="step"/>
        /// </summary>
        /// <param name="step">Step to use</param>
        public static float StepTo(this float f, float step)
        {
            return f - (f % step);
        }

        /// <summary>
        /// Compares a float with this float with an allowed offset
        /// </summary>
        /// <param name="b">Other float to compare</param>
        /// <param name="allowedDifference">Allowed offset</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool Compare(this float a, float b, float allowedDifference = 0.01f)
        {
            float absA = Mathf.Abs(a);
            float absB = Mathf.Abs(b);
            float diff = Mathf.Abs(a - b);

            if (Mathf.Abs(a - b) < allowedDifference)
            {
                return true;
            }
            else if (a == 0 || b == 0 || diff < float.MinValue)
            {
                return diff < (allowedDifference * float.MinValue);
            }
            else
            {
                return diff / Mathf.Min((absA + absB), float.MaxValue) < allowedDifference;
            }
        }

#if Mathematics

        #region Compare
        /// <summary>
        /// Unity.Mathematics to compare a float with this float with an allowed offset
        /// </summary>
        /// <param name="b">Other float to compare</param>
        /// <param name="allowedDifference">Allowed offset</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool MathCompare(this float a, float b, float allowedDifference = 0.01f)
        {
            float absA = math.abs(a);
            float absB = math.abs(b);
            float diff = math.abs(a - b);

            if (math.abs(a - b) < allowedDifference)
                return true;
            else if (a == 0 || b == 0 || diff < float.MinValue)
                return diff < (allowedDifference * float.MinValue);
            else
                return diff / math.min((absA + absB), float.MaxValue) <= allowedDifference;
        }

        /// <summary>
        /// Compares a float2 with this float2 with an allowed offset
        /// </summary>
        /// <param name="other">Other float2 to compare</param>
        /// <param name="allowedDifference">Allowed offset</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#if Burst
        public static bool Compare(this float2 vector, float2 other, float allowedDifference = 0.01f)
#else
        public static bool Compare(this float2 vector, in float2 other, float allowedDifference = 0.01f)
#endif
        {
            return math.distance(vector, other) <= allowedDifference;
        }

        /// <summary>
        /// Compares a float3 with this float3 with an allowed offset
        /// </summary>
        /// <param name="other">Other float3 to compare</param>
        /// <param name="allowedDifference">Allowed offset</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#if Burst
        public static bool Compare(this float3 vector, float3 other, float allowedDifference = 0.01f)
#else
        public static bool Compare(this float3 vector, in float3 other, float allowedDifference = 0.01f)
#endif
        {
            return math.distance(vector, other) <= allowedDifference;
        }

        /// <summary>
        /// Compares a float4 with this float4 with an allowed offset
        /// </summary>
        /// <param name="other">Other float4 to compare</param>
        /// <param name="allowedDifference">Allowed offset</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#if Burst
        public static bool Compare(this float4 vector, float4 other, float allowedDifference = 0.01f)
#else
        public static bool Compare(this float4 vector, in float4 other, float allowedDifference = 0.01f)
#endif
        {
            return math.distance(vector, other) <= allowedDifference;
        }
        #endregion Compare


        #region float2

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float2 WithX(this float2 value, float x) => new float2(x, value.y);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float2 WithY(this float2 value, float y) => new float2(value.x, y);

        #endregion float2

        #region float3

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float3 WithX(this float3 value, float x) => new float3(x, value.y, value.z);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float3 WithY(this float3 value, float y) => new float3(value.x, y, value.z);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float3 WithZ(this float3 value, float z) => new float3(value.x, value.y, z);

        #endregion

        #region float4

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float4 WithX(this float4 value, float x) => new float4(x, value.y, value.z, value.w);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float4 WithY(this float4 value, float y) => new float4(value.x, y, value.z, value.w);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float4 WithZ(this float4 value, float z) => new float4(value.x, value.y, z, value.w);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float4 WithW(this float4 value, float w) => new float4(value.x, value.y, value.z, w);

        #endregion float4

#endif
    }
}