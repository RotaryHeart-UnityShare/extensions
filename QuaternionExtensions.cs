﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Runtime.CompilerServices;
using UnityEngine;
#if Mathematics
using Unity.Mathematics;
#endif

namespace RotaryHeart.Lib.Extensions
{
    public static class QuaternionExtensions
    {

#if Mathematics
        /// <summary>
        /// Compares a quaternion with this quaternion with an allowed offset
        /// </summary>
        /// <param name="other">Other Vector4 to compare</param>
        /// <param name="allowedDifference">Allowed offset</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#if Burst
        public static bool Compare(this quaternion quaternion, quaternion other, float allowedDifference = 0.01f)
#else
        public static bool Compare(this quaternion quaternion, in quaternion other, float allowedDifference = 0.01f)
#endif
        {
            return math.abs(math.dot(quaternion, other)) >= 1 - allowedDifference;
        }
#endif

        /// <summary>
        /// Compares a Quaternion with this Quaternion with an allowed offset
        /// </summary>
        /// <param name="other">Other Vector4 to compare</param>
        /// <param name="allowedDifference">Allowed offset</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#if Burst
        public static bool Compare(this Quaternion quaternion, Quaternion other, float allowedDifference = 0.01f)
#else
        public static bool Compare(this Quaternion quaternion, in Quaternion other, float allowedDifference = 0.01f)
#endif
        {
            return Mathf.Abs(Quaternion.Dot(quaternion, other)) >= 1 - allowedDifference;
        }

    }
}