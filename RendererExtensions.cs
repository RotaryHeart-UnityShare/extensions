﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class RendererExtensions
    {

        /// <summary>
        /// Inserts a material <paramref name="mat"/> at index <paramref name="index"/>
        /// </summary>
        /// <param name="mat">Material to use</param>
        /// <param name="index">Index to insert the material in</param>
        public static void InsertMaterial(this Renderer renderer, Material mat, int index)
        {
            Material[] mats = renderer.materials;
            Material[] newMats = new Material[mats.Length + 1];

            int matIndex = 0;
            for (int i = 0; i < mats.Length; i++)
            {
                newMats[matIndex++] = i == index ? mat : mats[i];
            }

            if (index >= mats.Length)
                newMats[mats.Length] = mat;

            renderer.materials = newMats;
        }

        /// <summary>
        /// Replaces a material at index <paramref name="index"/> with material <paramref name="mat"/>
        /// </summary>
        /// <param name="mat">Material to use</param>
        /// <param name="index">Material index to replace</param>
        public static void ReplaceMaterial(this Renderer renderer, Material mat, int index)
        {
            Material[] mats = renderer.materials;
            mats[index] = mat;
            renderer.materials = mats;
        }

        /// <summary>
        /// Adds a new material to <paramref name="renderer"/>
        /// </summary>
        /// <param name="mat">Material to add</param>
        public static void AddMaterial(this Renderer renderer, Material mat)
        {
            Material[] mats = renderer.materials;
            Material[] newMats = new Material[mats.Length + 1];

            for (int i = 0; i < mats.Length; i++)
            {
                newMats[i] = mats[i];
            }

            newMats[mats.Length] = mat;
            renderer.materials = newMats;
        }

        /// <summary>
        /// Removes a material at the specified index
        /// </summary>
        /// <param name="index">Material index to remove</param>
        public static void RemoveMaterial(this Renderer renderer, int index = -1)
        {
            Material[] mats = renderer.materials;
            Material[] newMats = new Material[mats.Length - 1];

            if (index == -1)
                index = mats.Length - 1;

            int matIndex = 0;
            for (int i = 0; i < mats.Length; i++)
            {
                if (i == index)
                    continue;

                newMats[i] = mats[matIndex++];
            }

            renderer.materials = newMats;
        }

    }
}
