﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class TransformExtensions
    {
        /// <summary>
        /// Modifies the anchored position of this RectTransform
        /// </summary>
        /// <param name="left">Left value to set</param>
        /// <param name="top">Top value to set</param>
        /// <param name="right">Right value to set</param>
        /// <param name="bottom">Bottom value to set</param>
        public static void SetAnchoredRect(this RectTransform trs, float left, float top, float right, float bottom)
        {
            trs.offsetMin = new Vector2(left, bottom);
            trs.offsetMax = new Vector2(-right, -top);
        }

        /// <summary>
        /// Modifies the anchored left position of this RectTransform
        /// </summary>
        /// <param name="left">Left value to set</param>
        public static void SetLeft(this RectTransform trs, float left)
        {
            trs.offsetMin = new Vector2(left, trs.offsetMin.y);
        }

        /// <summary>
        /// Modifies the anchored right position of this RectTransform
        /// </summary>
        /// <param name="right">Right value to set</param>
        public static void SetRight(this RectTransform trs, float right)
        {
            trs.offsetMax = new Vector2(-right, trs.offsetMax.y);
        }

        /// <summary>
        /// Modifies the anchored top position of this RectTransform
        /// </summary>
        /// <param name="top">Top value to set</param>
        public static void SetTop(this RectTransform trs, float top)
        {
            trs.offsetMax = new Vector2(trs.offsetMax.x, -top);
        }

        /// <summary>
        /// Modifies the anchored bottom position of this RectTransform
        /// </summary>
        /// <param name="bottom">Bottom value to set</param>
        public static void SetBottom(this RectTransform trs, float bottom)
        {
            trs.offsetMin = new Vector2(trs.offsetMin.x, bottom);
        }

        /// <summary>
        /// Returns a world rect of the space the RectTransform is using
        /// </summary>
        public static Rect GetWorldRect(this RectTransform rectTransform)
        {
            Vector3[] corners = new Vector3[4];
            rectTransform.GetWorldCorners(corners);
            // Get the bottom left corner.
            Vector3 position = corners[0];

            Vector3 lossyScale = rectTransform.lossyScale;
            Rect rect = rectTransform.rect;
            Vector2 size = new Vector2(lossyScale.x * rect.size.x, lossyScale.y * rect.size.y);

            return new Rect(position, size);
        }

        /// <summary>
        /// Returns RectTransform position on screen
        /// </summary>
        /// <param name="cam">Camera used for calculating position</param>
        public static Rect GetScreenPosition(this RectTransform rectTransform, Camera cam)
        {
            Vector3[] corners = new Vector3[4];
            rectTransform.GetWorldCorners(corners);

            for (int i = 0; i < corners.Length; i++)
            {
                corners[i] = cam.WorldToScreenPoint(corners[i]);
            }

            //Top left position
            Vector2 position = (Vector2)corners[1];
            //Invert the y axis so top left corner = 0
            position.y = Screen.height - position.y;
            //Calculate size in pixel format
            Vector2 size = corners[2] - corners[0];

            return new Rect(position, size);
        }

        /// <summary>
        /// Destroys all the immediate children of this transform
        /// </summary>
        public static void DestroyAllChildren(this Transform trs)
        {
            int count = trs.childCount - 1;
            for (int i = count; i >= 0; i--)
                Object.Destroy(trs.GetChild(i).gameObject);
        }

        /// <summary>
        /// Destroys all the immediate children of this transform. NOTE using DestroyImmediate
        /// </summary>
        public static void DestroyImmediateAllChildren(this Transform trs)
        {
            int count = trs.childCount - 1;
            for (int i = count; i >= 0; i--)
                Object.DestroyImmediate(trs.GetChild(i).gameObject);
        }

        /// <summary>
        /// Returns the degrees of a world coord using this transform as the center
        /// </summary>
        /// <param name="worldCoords">World coord to check</param>
        /// <param name="axis">What axis are going to be used for the calculation</param>
        public static float GetWorldDegree(this Transform transform, Vector3 worldCoords, Axis axis = Axis.XY)
        {
            return transform.GetLocalRadiant(transform.InverseTransformPoint(worldCoords), axis) * Mathf.Rad2Deg;
        }

        /// <summary>
        /// Returns the degrees of a local coord using this transform as the center
        /// </summary>
        /// <param name="localCoords">Local coord to check</param>
        /// <param name="axis">What axis are going to be used for the calculation</param>
        public static float GetLocalDegree(this Transform transform, Vector3 localCoords, Axis axis = Axis.XY)
        {
            return transform.GetLocalRadiant(localCoords, axis) * Mathf.Rad2Deg;
        }

        /// <summary>
        /// Returns the radiant of a world coord using this transform as the center
        /// </summary>
        /// <param name="worldCoords">World coord to check</param>
        /// <param name="axis">What axis are going to be used for the calculation</param>
        public static float GetWorldRadiant(this Transform transform, Vector3 worldCoords, Axis axis = Axis.XY)
        {
            return transform.GetLocalRadiant(transform.InverseTransformPoint(worldCoords), axis);
        }

        /// <summary>
        /// Returns the radiant of a local coord using this transform as the center
        /// </summary>
        /// <param name="localCoords">Local coord to check</param>
        /// <param name="axis">What axis are going to be used for the calculation</param>
        public static float GetLocalRadiant(this Transform transform, Vector3 localCoords, Axis axis = Axis.XY)
        {
            float radiant = 0;

            switch (axis)
            {
                case Axis.XY:
                    radiant = Mathf.Atan2(localCoords.y, localCoords.x);
                    break;

                case Axis.XZ:
                    radiant = Mathf.Atan2(localCoords.z, localCoords.x);
                    break;

                case Axis.YX:
                    radiant = Mathf.Atan2(localCoords.x, localCoords.y);
                    break;

                case Axis.YZ:
                    radiant = Mathf.Atan2(localCoords.z, localCoords.y);
                    break;

                case Axis.ZX:
                    radiant = Mathf.Atan2(localCoords.x, localCoords.z);
                    break;

                case Axis.ZY:
                    radiant = Mathf.Atan2(localCoords.y, localCoords.z);
                    break;
            }

            float fullRadiant = 2 * Mathf.PI;

            radiant = (fullRadiant + radiant) % fullRadiant;

            return radiant;
        }

        /// <summary>
        /// Returns a normalized Vector pointing towards the angle based on the axis
        /// </summary>
        /// <param name="degrees">Angle in degrees to check</param>
        /// <param name="axis">What axis are going to be used for the calculation</param>
        /// <returns></returns>
        public static Vector3 GetDegreeDirection(this Transform transform, float degrees, Axis axis = Axis.XY)
        {
            Vector3 direction = Vector3.zero;

            switch (axis)
            {
                case Axis.XY:
                    direction.x = Mathf.Cos(degrees);
                    direction.y = Mathf.Sin(degrees);
                    break;

                case Axis.XZ:
                    direction.x = Mathf.Cos(degrees);
                    direction.z = Mathf.Sin(degrees);
                    break;

                case Axis.YX:
                    direction.y = Mathf.Cos(degrees);
                    direction.x = Mathf.Sin(degrees);
                    break;

                case Axis.YZ:
                    direction.y = Mathf.Cos(degrees);
                    direction.z = Mathf.Sin(degrees);
                    break;

                case Axis.ZX:
                    direction.z = Mathf.Cos(degrees);
                    direction.x = Mathf.Sin(degrees);
                    break;

                case Axis.ZY:
                    direction.z = Mathf.Cos(degrees);
                    direction.y = Mathf.Sin(degrees);
                    break;
            }

            return direction;
        }

        /// <summary>
        /// Returns a normalized Vector pointing towards the angle based on the axis
        /// </summary>
        /// <param name="radiants">Angle in radiant to check</param>
        /// <param name="axis">What axis are going to be used for the calculation</param>
        /// <returns></returns>
        public static Vector3 GetRadiantDirection(this Transform transform, float radiants, Axis axis = Axis.XY)
        {
            return transform.GetDegreeDirection(radiants, axis) * Mathf.Deg2Rad;
        }

    }
}