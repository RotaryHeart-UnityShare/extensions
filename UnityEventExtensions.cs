// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Reflection;
using UnityEngine.Events;

namespace RotaryHeart.Lib.Extensions
{
    public static class UnityEventExtensions
    {
        /// <summary>
        /// Returns how many listeners a unity event has. NOTE Uses reflection so should be used with care
        /// </summary>
        public static int GetListenerCount(this UnityEventBase unityEvent)
        {
            FieldInfo field = typeof(UnityEventBase).GetField("m_Calls", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
            object invokeCallList = field.GetValue(unityEvent);
            PropertyInfo property = invokeCallList.GetType().GetProperty("Count");
            return (int)property.GetValue(invokeCallList);
        }
    }
}