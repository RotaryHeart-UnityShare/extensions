﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Runtime.CompilerServices;
using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class Vector4Extensions
    {

        #region Clamping

        /// <summary>
        /// Clamps the Vector4 individual values
        /// </summary>
        /// <param name="xRange">Range for vector x</param>
        /// <param name="yRange">Range for vector y</param>
        /// <param name="zRange">Range for vector z</param>
        /// <param name="wRange">Range for vector w</param>
        public static Vector4 Clamp(this Vector4 vector, Vector2 xRange, Vector2 yRange, Vector2 zRange, Vector2 wRange)
        {
            return vector.Clamp(xRange.x, xRange.y, yRange.x, yRange.y, zRange.x, zRange.y, wRange.x, wRange.y);
        }
        /// <summary>
        /// Clamps the Vector4 individual values
        /// </summary>
        /// <param name="xMin">Minimum value for vector x</param>
        /// <param name="xMax">Maximum value for vector x</param>
        /// <param name="yMin">Minimum value for vector y</param>
        /// <param name="yMax">Maximum value for vector y</param>
        /// <param name="zMin">Minimum value for vector z</param>
        /// <param name="zMax">Maximum value for vector z</param>
        /// <param name="wMin">Minimum value for vector w</param>
        /// <param name="wMax">Maximum value for vector w</param>
        public static Vector4 Clamp(this Vector4 vector, float xMin, float xMax, float yMin, float yMax, float zMin, float zMax, float wMin, float wMax)
        {
            float x = Mathf.Clamp(vector.x, xMin, xMax);
            float y = Mathf.Clamp(vector.y, yMin, yMax);
            float z = Mathf.Clamp(vector.z, zMin, zMax);
            float w = Mathf.Clamp(vector.w, wMin, wMax);

            return new Vector4(x, y, z, w);
        }
        /// <summary>
        /// Clamps the Vector4 x value
        /// </summary>
        /// <param name="range">Range for vector x</param>
        public static Vector4 ClampX(this Vector4 vector, Vector2 range)
        {
            return vector.ClampX(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector4 x value
        /// </summary>
        /// <param name="min">Minimum value for vector x</param>
        /// <param name="max">Maximum value for vector x</param>
        public static Vector4 ClampX(this Vector4 vector, float min, float max)
        {
            float x = Mathf.Clamp(vector.x, min, max);

            return vector.WithX(x);
        }
        /// <summary>
        /// Clamps the Vector4 y value
        /// </summary>
        /// <param name="range">Range for vector y</param>
        public static Vector4 ClampY(this Vector4 vector, Vector2 range)
        {
            return vector.ClampY(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector4 y value
        /// </summary>
        /// <param name="min">Minimum value for vector y</param>
        /// <param name="max">Maximum value for vector y</param>
        public static Vector4 ClampY(this Vector4 vector, float min, float max)
        {
            float y = Mathf.Clamp(vector.y, min, max);

            return vector.WithY(y);
        }
        /// <summary>
        /// Clamps the Vector4 z value
        /// </summary>
        /// <param name="range">Range for vector z</param>
        public static Vector4 ClampZ(this Vector4 vector, Vector2 range)
        {
            return vector.ClampZ(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector4 z value
        /// </summary>
        /// <param name="min">Minimum value for vector z</param>
        /// <param name="max">Maximum value for vector z</param>
        public static Vector4 ClampZ(this Vector4 vector, float min, float max)
        {
            float z = Mathf.Clamp(vector.z, min, max);

            return vector.WithZ(z);
        }
        /// <summary>
        /// Clamps the Vector4 w value
        /// </summary>
        /// <param name="range">Range for vector w</param>
        public static Vector4 ClampW(this Vector4 vector, Vector2 range)
        {
            return vector.ClampW(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector4 w value
        /// </summary>
        /// <param name="min">Minimum value for vector w</param>
        /// <param name="max">Maximum value for vector w</param>
        public static Vector4 ClampW(this Vector4 vector, float min, float max)
        {
            float w = Mathf.Clamp(vector.w, min, max);

            return vector.WithW(w);
        }

        /// <summary>
        /// Returns a copy of vector with its magnitude clamped between min and max length. Note that if the magnitude is 0
        /// it will not be clamped
        /// </summary>
        /// <param name="min">Minimum magnitude value</param>
        /// <param name="max">Maximum magnitude value</param>
        public static Vector4 ClampMagnitude(this Vector4 vector, float min, float max)
        {
            float sm = vector.sqrMagnitude;
            if (sm > max * max)
            {
                return vector.normalized * max;
            }
            if (sm > 0 && sm < min * min)
            {
                return vector.normalized * min;
            }
            return vector;
        }

        #endregion Clamping

        /// <summary>
        /// Compares a Vector4 with this Vector4 with an allowed offset
        /// </summary>
        /// <param name="other">Other Vector4 to compare</param>
        /// <param name="allowedDifference">Allowed offset</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#if Burst
        public static bool Compare(this Vector4 vector, Vector4 other, float allowedDifference = 0.01f)
#else
        public static bool Compare(this Vector4 vector, in Vector4 other, float allowedDifference = 0.01f)
#endif
        {
            return (vector - other).sqrMagnitude <= allowedDifference;
        }

        /// <summary>
        /// Returns the Vector4 with X value of <paramref name="x"/>
        /// </summary>
        /// <param name="x">Value to use</param>
        public static Vector4 WithX(this Vector4 vector, float x) => new Vector4(x, vector.y, vector.z, vector.w);
        /// <summary>
        /// Returns the Vector4 with Y value of <paramref name="y"/>
        /// </summary>
        /// <param name="y">Value to use</param>
        public static Vector4 WithY(this Vector4 vector, float y) => new Vector4(vector.x, y, vector.z, vector.w);
        /// <summary>
        /// Returns the Vector4 with Z value of <paramref name="z"/>
        /// </summary>
        /// <param name="z">Value to use</param>
        public static Vector4 WithZ(this Vector4 vector, float z) => new Vector4(vector.x, vector.y, z, vector.w);
        /// <summary>
        /// Returns the Vector4 with W value of <paramref name="w"/>
        /// </summary>
        /// <param name="w">Value to use</param>
        public static Vector4 WithW(this Vector4 vector, float w) => new Vector4(vector.x, vector.y, vector.z, w);
        /// <summary>
        /// Returns the absolute value of the vector
        /// </summary>
        public static Vector4 Abs(this Vector4 vector) => new Vector4(Mathf.Abs(vector.x), Mathf.Abs(vector.y), Mathf.Abs(vector.z), Mathf.Abs(vector.w));
        /// <summary>
        /// Ensures the vector value is a multiple of <paramref name="step"/>
        /// </summary>
        /// <param name="step">Step to use</param>
        public static Vector4 StepTo(this Vector4 vector, float step) => new Vector4(vector.x.StepTo(step), vector.y.StepTo(step), vector.z.StepTo(step), vector.w.StepTo(step));
        /// <summary>
        /// Ensures the vector value is a multiple of <paramref name="step"/>
        /// </summary>
        /// <param name="step">Step to use</param>
        public static Vector4 StepTo(this Vector4 vector, Vector4 step) => new Vector4(vector.x.StepTo(step.x), vector.y.StepTo(step.y), vector.z.StepTo(step.z), vector.w.StepTo(step.w));
        /// <summary>
        /// Returns a vector with random values multiplied by x, y, z, and w
        /// </summary>
        /// <param name="min">Min range for the random value</param>
        /// <param name="max">Max range for the random value</param>
        public static Vector4 Random(this Vector4 vector, float min, float max) => new Vector4(vector.x * UnityEngine.Random.Range(min, max), vector.y * UnityEngine.Random.Range(min, max), vector.z * UnityEngine.Random.Range(min, max), vector.w * UnityEngine.Random.Range(min, max));
        /// <summary>
        /// Returns a vector with random values
        /// </summary>
        /// <param name="min">Min range for the random value</param>
        /// <param name="max">Max range for the random value</param>
        public static Vector4 Random(float min, float max) => new Vector4(UnityEngine.Random.Range(min, max), UnityEngine.Random.Range(min, max), UnityEngine.Random.Range(min, max), UnityEngine.Random.Range(min, max));
        /// <summary>
        /// Indicates if the vector has any NaN values
        /// </summary>
        public static bool IsNaN(this Vector4 vector) => float.IsNaN(vector.x) || float.IsNaN(vector.y) || float.IsNaN(vector.z) || float.IsNaN(vector.w);

        #region Mathematics operations

        /// <summary>
        /// Returns the Vector4 with <paramref name="x"/> added to the X value
        /// </summary>
        /// <param name="x">Value to add to X</param>
        public static Vector4 AddToX(this Vector4 vector, float x) => new Vector4(vector.x + x, vector.y, vector.z, vector.w);
        /// <summary>
        /// Returns the Vector4 with <paramref name="y"/> added to the Y value
        /// </summary>
        /// <param name="y">Value to add to Y</param>
        public static Vector4 AddToY(this Vector4 vector, float y) => new Vector4(vector.x, vector.y + y, vector.z, vector.w);
        /// <summary>
        /// Returns the Vector4 with <paramref name="z"/> added to the Z value
        /// </summary>
        /// <param name="z">Value to add to Z</param>
        public static Vector4 AddToZ(this Vector4 vector, float z) => new Vector4(vector.x, vector.y, vector.z + z, vector.w);
        /// <summary>
        /// Returns the Vector4 with <paramref name="height"/> added to the W value
        /// </summary>
        /// <param name="height">Value to add to W</param>
        public static Vector4 AddToW(this Vector4 vector, float height) => new Vector4(vector.x, vector.y, vector.z, vector.w + height);
        /// <summary>
        /// Returns the Vector4 with <paramref name="x"/> subtracted to the X value
        /// </summary>
        /// <param name="x">Value to subtract to X</param>
        public static Vector4 SubtractToX(this Vector4 vector, float x) => new Vector4(vector.x - x, vector.y, vector.z, vector.w);
        /// <summary>
        /// Returns the Vector4 with <paramref name="y"/> subtracted to the Y value
        /// </summary>
        /// <param name="y">Value to subtract to Y</param>
        public static Vector4 SubtractToY(this Vector4 vector, float y) => new Vector4(vector.x, vector.y - y, vector.z, vector.w);
        /// <summary>
        /// Returns the Vector4 with <paramref name="width"/> subtracted to the Z value
        /// </summary>
        /// <param name="width">Value to subtract to Z</param>
        public static Vector4 SubtractToZ(this Vector4 vector, float width) => new Vector4(vector.x, vector.y, vector.z - width, vector.w);
        /// <summary>
        /// Returns the Vector4 with <paramref name="height"/> subtracted to the W value
        /// </summary>
        /// <param name="height">Value to subtract to W</param>
        public static Vector4 SubtractToW(this Vector4 vector, float height) => new Vector4(vector.x, vector.y, vector.z, vector.w - height);
        /// <summary>
        /// Returns the Vector4 with <paramref name="x"/> multiplied to the X value
        /// </summary>
        /// <param name="x">Value to multiply to X</param>
        public static Vector4 MultiplyToX(this Vector4 vector, float x) => new Vector4(vector.x * x, vector.y, vector.z, vector.w);
        /// <summary>
        /// Returns the Vector4 with <paramref name="y"/> multiplied to the Y value
        /// </summary>
        /// <param name="y">Value to multiply to Y</param>
        public static Vector4 MultiplyToY(this Vector4 vector, float y) => new Vector4(vector.x, vector.y * y, vector.z, vector.w);
        /// <summary>
        /// Returns the Vector4 with <paramref name="width"/> multiplied to the Z value
        /// </summary>
        /// <param name="width">Value to multiply to Z</param>
        public static Vector4 MultiplyToZ(this Vector4 vector, float width) => new Vector4(vector.x, vector.y, vector.z * width, vector.w);
        /// <summary>
        /// Returns the Vector4 with <paramref name="height"/> multiplied to the W value
        /// </summary>
        /// <param name="height">Value to multiply to W</param>
        public static Vector4 MultiplyToW(this Vector4 vector, float height) => new Vector4(vector.x, vector.y, vector.z, vector.w * height);
        /// <summary>
        /// Returns the Vector4 with <paramref name="x"/> divided to the X value
        /// </summary>
        /// <param name="x">Value to divide to X</param>
        public static Vector4 DivideToX(this Vector4 vector, float x) => new Vector4(vector.x / x, vector.y, vector.z, vector.w);
        /// <summary>
        /// Returns the Vector4 with <paramref name="y"/> divided to the Y value
        /// </summary>
        /// <param name="y">Value to divide to Y</param>
        public static Vector4 DivideToY(this Vector4 vector, float y) => new Vector4(vector.x, vector.y / y, vector.z, vector.w);
        /// <summary>
        /// Returns the Vector4 with <paramref name="width"/> divided to the Z value
        /// </summary>
        /// <param name="width">Value to divide to Z</param>
        public static Vector4 DivideToZ(this Vector4 vector, float width) => new Vector4(vector.x, vector.y, vector.z / width, vector.w);
        /// <summary>
        /// Returns the Vector4 with <paramref name="height"/> divided to the W value
        /// </summary>
        /// <param name="height">Value to divide to W</param>
        public static Vector4 DivideToW(this Vector4 vector, float height) => new Vector4(vector.x, vector.y, vector.z, vector.w / height);

        #endregion Mathematics operations

        #region Conversions

        public static Vector2 xx(this Vector4 vector) => new Vector2(vector.x, vector.x);
        public static Vector2 xy(this Vector4 vector) => new Vector2(vector.x, vector.y);
        public static Vector2 xz(this Vector4 vector) => new Vector2(vector.x, vector.z);
        public static Vector2 xw(this Vector4 vector) => new Vector2(vector.x, vector.w);
        public static Vector2 yx(this Vector4 vector) => new Vector2(vector.y, vector.x);
        public static Vector2 yy(this Vector4 vector) => new Vector2(vector.y, vector.y);
        public static Vector2 yz(this Vector4 vector) => new Vector2(vector.y, vector.z);
        public static Vector2 yw(this Vector4 vector) => new Vector2(vector.y, vector.w);
        public static Vector2 zx(this Vector4 vector) => new Vector2(vector.z, vector.x);
        public static Vector2 zy(this Vector4 vector) => new Vector2(vector.z, vector.y);
        public static Vector2 zz(this Vector4 vector) => new Vector2(vector.z, vector.z);
        public static Vector2 zw(this Vector4 vector) => new Vector2(vector.z, vector.w);
        public static Vector2 wx(this Vector4 vector) => new Vector2(vector.w, vector.x);
        public static Vector2 wy(this Vector4 vector) => new Vector2(vector.w, vector.y);
        public static Vector2 wz(this Vector4 vector) => new Vector2(vector.w, vector.z);
        public static Vector2 ww(this Vector4 vector) => new Vector2(vector.w, vector.w);
        public static Vector3 xxx(this Vector4 vector) => new Vector3(vector.x, vector.x, vector.x);
        public static Vector3 xxy(this Vector4 vector) => new Vector3(vector.x, vector.x, vector.y);
        public static Vector3 xxz(this Vector4 vector) => new Vector3(vector.x, vector.x, vector.z);
        public static Vector3 xxw(this Vector4 vector) => new Vector3(vector.x, vector.x, vector.w);
        public static Vector3 xyx(this Vector4 vector) => new Vector3(vector.x, vector.y, vector.x);
        public static Vector3 xyy(this Vector4 vector) => new Vector3(vector.x, vector.y, vector.y);
        public static Vector3 xyz(this Vector4 vector) => new Vector3(vector.x, vector.y, vector.z);
        public static Vector3 xyw(this Vector4 vector) => new Vector3(vector.x, vector.y, vector.w);
        public static Vector3 xzx(this Vector4 vector) => new Vector3(vector.x, vector.z, vector.x);
        public static Vector3 xzy(this Vector4 vector) => new Vector3(vector.x, vector.z, vector.y);
        public static Vector3 xzz(this Vector4 vector) => new Vector3(vector.x, vector.z, vector.z);
        public static Vector3 xzw(this Vector4 vector) => new Vector3(vector.x, vector.z, vector.w);
        public static Vector3 xwx(this Vector4 vector) => new Vector3(vector.x, vector.w, vector.x);
        public static Vector3 xwy(this Vector4 vector) => new Vector3(vector.x, vector.w, vector.y);
        public static Vector3 xwz(this Vector4 vector) => new Vector3(vector.x, vector.w, vector.z);
        public static Vector3 xww(this Vector4 vector) => new Vector3(vector.x, vector.w, vector.w);
        public static Vector3 yxx(this Vector4 vector) => new Vector3(vector.y, vector.x, vector.x);
        public static Vector3 yxy(this Vector4 vector) => new Vector3(vector.y, vector.x, vector.y);
        public static Vector3 yxz(this Vector4 vector) => new Vector3(vector.y, vector.x, vector.z);
        public static Vector3 yxw(this Vector4 vector) => new Vector3(vector.y, vector.x, vector.w);
        public static Vector3 yyx(this Vector4 vector) => new Vector3(vector.y, vector.y, vector.x);
        public static Vector3 yyy(this Vector4 vector) => new Vector3(vector.y, vector.y, vector.y);
        public static Vector3 yyz(this Vector4 vector) => new Vector3(vector.y, vector.y, vector.z);
        public static Vector3 yyw(this Vector4 vector) => new Vector3(vector.y, vector.y, vector.w);
        public static Vector3 yzx(this Vector4 vector) => new Vector3(vector.y, vector.z, vector.x);
        public static Vector3 yzy(this Vector4 vector) => new Vector3(vector.y, vector.z, vector.y);
        public static Vector3 yzz(this Vector4 vector) => new Vector3(vector.y, vector.z, vector.z);
        public static Vector3 yzw(this Vector4 vector) => new Vector3(vector.y, vector.z, vector.w);
        public static Vector3 ywx(this Vector4 vector) => new Vector3(vector.y, vector.w, vector.x);
        public static Vector3 ywy(this Vector4 vector) => new Vector3(vector.y, vector.w, vector.y);
        public static Vector3 ywz(this Vector4 vector) => new Vector3(vector.y, vector.w, vector.z);
        public static Vector3 yww(this Vector4 vector) => new Vector3(vector.y, vector.w, vector.w);
        public static Vector3 zxx(this Vector4 vector) => new Vector3(vector.w, vector.x, vector.x);
        public static Vector3 zxy(this Vector4 vector) => new Vector3(vector.w, vector.x, vector.y);
        public static Vector3 zxz(this Vector4 vector) => new Vector3(vector.w, vector.x, vector.z);
        public static Vector3 zxw(this Vector4 vector) => new Vector3(vector.w, vector.x, vector.w);
        public static Vector3 zyx(this Vector4 vector) => new Vector3(vector.w, vector.y, vector.x);
        public static Vector3 zyy(this Vector4 vector) => new Vector3(vector.w, vector.y, vector.y);
        public static Vector3 zyz(this Vector4 vector) => new Vector3(vector.w, vector.y, vector.z);
        public static Vector3 zyw(this Vector4 vector) => new Vector3(vector.w, vector.y, vector.w);
        public static Vector3 zzx(this Vector4 vector) => new Vector3(vector.w, vector.z, vector.x);
        public static Vector3 zzy(this Vector4 vector) => new Vector3(vector.w, vector.z, vector.y);
        public static Vector3 zzz(this Vector4 vector) => new Vector3(vector.w, vector.z, vector.z);
        public static Vector3 zzw(this Vector4 vector) => new Vector3(vector.w, vector.z, vector.w);
        public static Vector3 zwx(this Vector4 vector) => new Vector3(vector.w, vector.w, vector.x);
        public static Vector3 zwy(this Vector4 vector) => new Vector3(vector.w, vector.w, vector.y);
        public static Vector3 zwz(this Vector4 vector) => new Vector3(vector.w, vector.w, vector.z);
        public static Vector3 zww(this Vector4 vector) => new Vector3(vector.w, vector.w, vector.w);
        public static Vector3 wxx(this Vector4 vector) => new Vector3(vector.w, vector.x, vector.x);
        public static Vector3 wxy(this Vector4 vector) => new Vector3(vector.w, vector.x, vector.y);
        public static Vector3 wxz(this Vector4 vector) => new Vector3(vector.w, vector.x, vector.z);
        public static Vector3 wxw(this Vector4 vector) => new Vector3(vector.w, vector.x, vector.w);
        public static Vector3 wyx(this Vector4 vector) => new Vector3(vector.w, vector.y, vector.x);
        public static Vector3 wyy(this Vector4 vector) => new Vector3(vector.w, vector.y, vector.y);
        public static Vector3 wyz(this Vector4 vector) => new Vector3(vector.w, vector.y, vector.z);
        public static Vector3 wyw(this Vector4 vector) => new Vector3(vector.w, vector.y, vector.w);
        public static Vector3 wzx(this Vector4 vector) => new Vector3(vector.w, vector.z, vector.x);
        public static Vector3 wzy(this Vector4 vector) => new Vector3(vector.w, vector.z, vector.y);
        public static Vector3 wzz(this Vector4 vector) => new Vector3(vector.w, vector.z, vector.z);
        public static Vector3 wzw(this Vector4 vector) => new Vector3(vector.w, vector.z, vector.w);
        public static Vector3 wwx(this Vector4 vector) => new Vector3(vector.w, vector.w, vector.x);
        public static Vector3 wwy(this Vector4 vector) => new Vector3(vector.w, vector.w, vector.y);
        public static Vector3 wwz(this Vector4 vector) => new Vector3(vector.w, vector.w, vector.z);
        public static Vector3 www(this Vector4 vector) => new Vector3(vector.w, vector.w, vector.w);
        public static Vector4 xxxx(this Vector4 vector) => new Vector4(vector.x, vector.x, vector.x, vector.x);
        public static Vector4 xxxy(this Vector4 vector) => new Vector4(vector.x, vector.x, vector.x, vector.y);
        public static Vector4 xxxz(this Vector4 vector) => new Vector4(vector.x, vector.x, vector.x, vector.z);
        public static Vector4 xxxw(this Vector4 vector) => new Vector4(vector.x, vector.x, vector.x, vector.w);
        public static Vector4 xxyx(this Vector4 vector) => new Vector4(vector.x, vector.x, vector.y, vector.x);
        public static Vector4 xxyy(this Vector4 vector) => new Vector4(vector.x, vector.x, vector.y, vector.y);
        public static Vector4 xxyz(this Vector4 vector) => new Vector4(vector.x, vector.x, vector.y, vector.z);
        public static Vector4 xxyw(this Vector4 vector) => new Vector4(vector.x, vector.x, vector.y, vector.w);
        public static Vector4 xxzx(this Vector4 vector) => new Vector4(vector.x, vector.x, vector.z, vector.x);
        public static Vector4 xxzy(this Vector4 vector) => new Vector4(vector.x, vector.x, vector.z, vector.y);
        public static Vector4 xxzz(this Vector4 vector) => new Vector4(vector.x, vector.x, vector.z, vector.z);
        public static Vector4 xxzw(this Vector4 vector) => new Vector4(vector.x, vector.x, vector.z, vector.w);
        public static Vector4 xyxx(this Vector4 vector) => new Vector4(vector.x, vector.y, vector.x, vector.x);
        public static Vector4 xyxy(this Vector4 vector) => new Vector4(vector.x, vector.y, vector.x, vector.y);
        public static Vector4 xyxz(this Vector4 vector) => new Vector4(vector.x, vector.y, vector.x, vector.z);
        public static Vector4 xyxw(this Vector4 vector) => new Vector4(vector.x, vector.y, vector.x, vector.w);
        public static Vector4 xyyx(this Vector4 vector) => new Vector4(vector.x, vector.y, vector.y, vector.x);
        public static Vector4 xyyy(this Vector4 vector) => new Vector4(vector.x, vector.y, vector.y, vector.y);
        public static Vector4 xyyz(this Vector4 vector) => new Vector4(vector.x, vector.y, vector.y, vector.z);
        public static Vector4 xyyw(this Vector4 vector) => new Vector4(vector.x, vector.y, vector.y, vector.w);
        public static Vector4 xzxx(this Vector4 vector) => new Vector4(vector.x, vector.z, vector.x, vector.x);
        public static Vector4 xzxy(this Vector4 vector) => new Vector4(vector.x, vector.z, vector.x, vector.y);
        public static Vector4 xzxz(this Vector4 vector) => new Vector4(vector.x, vector.z, vector.x, vector.z);
        public static Vector4 xzxw(this Vector4 vector) => new Vector4(vector.x, vector.z, vector.x, vector.w);
        public static Vector4 xzyx(this Vector4 vector) => new Vector4(vector.x, vector.z, vector.y, vector.x);
        public static Vector4 xzyy(this Vector4 vector) => new Vector4(vector.x, vector.z, vector.y, vector.y);
        public static Vector4 xzyz(this Vector4 vector) => new Vector4(vector.x, vector.z, vector.y, vector.z);
        public static Vector4 xzyw(this Vector4 vector) => new Vector4(vector.x, vector.z, vector.y, vector.w);
        public static Vector4 xzzx(this Vector4 vector) => new Vector4(vector.x, vector.z, vector.z, vector.x);
        public static Vector4 xzzy(this Vector4 vector) => new Vector4(vector.x, vector.z, vector.z, vector.y);
        public static Vector4 xzzz(this Vector4 vector) => new Vector4(vector.x, vector.z, vector.z, vector.z);
        public static Vector4 xzzw(this Vector4 vector) => new Vector4(vector.x, vector.z, vector.z, vector.w);
        public static Vector4 xwxx(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.x, vector.x);
        public static Vector4 xwxy(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.x, vector.y);
        public static Vector4 xwxz(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.x, vector.z);
        public static Vector4 xwxw(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.x, vector.w);
        public static Vector4 xwyx(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.y, vector.x);
        public static Vector4 xwyy(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.y, vector.y);
        public static Vector4 xwyz(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.y, vector.z);
        public static Vector4 xwyw(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.y, vector.w);
        public static Vector4 xwzx(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.z, vector.x);
        public static Vector4 xwzy(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.z, vector.y);
        public static Vector4 xwzz(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.z, vector.z);
        public static Vector4 xwzw(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.z, vector.w);
        public static Vector4 xwwx(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.w, vector.x);
        public static Vector4 xwwy(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.w, vector.y);
        public static Vector4 xwwz(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.w, vector.z);
        public static Vector4 xwww(this Vector4 vector) => new Vector4(vector.x, vector.w, vector.w, vector.w);
        public static Vector3 yxxx(this Vector4 vector) => new Vector4(vector.y, vector.x, vector.x, vector.x);
        public static Vector3 yxxy(this Vector4 vector) => new Vector4(vector.y, vector.x, vector.x, vector.y);
        public static Vector3 yxxz(this Vector4 vector) => new Vector4(vector.y, vector.x, vector.x, vector.z);
        public static Vector3 yxxw(this Vector4 vector) => new Vector4(vector.y, vector.x, vector.x, vector.w);
        public static Vector3 yxyx(this Vector4 vector) => new Vector4(vector.y, vector.x, vector.y, vector.x);
        public static Vector3 yxyy(this Vector4 vector) => new Vector4(vector.y, vector.x, vector.y, vector.y);
        public static Vector3 yxyz(this Vector4 vector) => new Vector4(vector.y, vector.x, vector.y, vector.z);
        public static Vector3 yxyw(this Vector4 vector) => new Vector4(vector.y, vector.x, vector.y, vector.w);
        public static Vector3 yxzx(this Vector4 vector) => new Vector4(vector.y, vector.x, vector.z, vector.x);
        public static Vector3 yxzy(this Vector4 vector) => new Vector4(vector.y, vector.x, vector.z, vector.y);
        public static Vector3 yxzz(this Vector4 vector) => new Vector4(vector.y, vector.x, vector.z, vector.z);
        public static Vector3 yxzw(this Vector4 vector) => new Vector4(vector.y, vector.x, vector.z, vector.w);
        public static Vector3 yyxx(this Vector4 vector) => new Vector4(vector.y, vector.y, vector.x, vector.x);
        public static Vector3 yyxy(this Vector4 vector) => new Vector4(vector.y, vector.y, vector.x, vector.y);
        public static Vector3 yyxz(this Vector4 vector) => new Vector4(vector.y, vector.y, vector.x, vector.z);
        public static Vector3 yyxw(this Vector4 vector) => new Vector4(vector.y, vector.y, vector.x, vector.w);
        public static Vector3 yyyx(this Vector4 vector) => new Vector4(vector.y, vector.y, vector.y, vector.x);
        public static Vector3 yyyy(this Vector4 vector) => new Vector4(vector.y, vector.y, vector.y, vector.y);
        public static Vector3 yyyz(this Vector4 vector) => new Vector4(vector.y, vector.y, vector.y, vector.z);
        public static Vector3 yyyw(this Vector4 vector) => new Vector4(vector.y, vector.y, vector.y, vector.w);
        public static Vector3 yzxx(this Vector4 vector) => new Vector4(vector.y, vector.z, vector.x, vector.x);
        public static Vector3 yzxy(this Vector4 vector) => new Vector4(vector.y, vector.z, vector.x, vector.y);
        public static Vector3 yzxz(this Vector4 vector) => new Vector4(vector.y, vector.z, vector.x, vector.z);
        public static Vector3 yzxw(this Vector4 vector) => new Vector4(vector.y, vector.z, vector.x, vector.w);
        public static Vector3 yzyx(this Vector4 vector) => new Vector4(vector.y, vector.z, vector.y, vector.x);
        public static Vector3 yzyy(this Vector4 vector) => new Vector4(vector.y, vector.z, vector.y, vector.y);
        public static Vector3 yzyz(this Vector4 vector) => new Vector4(vector.y, vector.z, vector.y, vector.z);
        public static Vector3 yzyw(this Vector4 vector) => new Vector4(vector.y, vector.z, vector.y, vector.w);
        public static Vector3 yzzx(this Vector4 vector) => new Vector4(vector.y, vector.z, vector.z, vector.x);
        public static Vector3 yzzy(this Vector4 vector) => new Vector4(vector.y, vector.z, vector.z, vector.y);
        public static Vector3 yzzz(this Vector4 vector) => new Vector4(vector.y, vector.z, vector.z, vector.z);
        public static Vector3 yzzw(this Vector4 vector) => new Vector4(vector.y, vector.z, vector.z, vector.w);
        public static Vector3 ywxx(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.x, vector.x);
        public static Vector3 ywxy(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.x, vector.y);
        public static Vector3 ywxz(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.x, vector.z);
        public static Vector3 ywxw(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.x, vector.w);
        public static Vector3 ywyx(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.y, vector.x);
        public static Vector3 ywyy(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.y, vector.y);
        public static Vector3 ywyz(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.y, vector.z);
        public static Vector3 ywyw(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.y, vector.w);
        public static Vector3 ywzx(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.z, vector.x);
        public static Vector3 ywzy(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.z, vector.y);
        public static Vector3 ywzz(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.z, vector.z);
        public static Vector3 ywzw(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.z, vector.w);
        public static Vector3 ywwx(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.w, vector.x);
        public static Vector3 ywwy(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.w, vector.y);
        public static Vector3 ywwz(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.w, vector.z);
        public static Vector3 ywww(this Vector4 vector) => new Vector4(vector.y, vector.w, vector.w, vector.w);
        public static Vector3 zxxx(this Vector4 vector) => new Vector4(vector.z, vector.x, vector.x, vector.x);
        public static Vector3 zxxy(this Vector4 vector) => new Vector4(vector.z, vector.x, vector.x, vector.y);
        public static Vector3 zxxz(this Vector4 vector) => new Vector4(vector.z, vector.x, vector.x, vector.z);
        public static Vector3 zxxw(this Vector4 vector) => new Vector4(vector.z, vector.x, vector.x, vector.w);
        public static Vector3 zxyx(this Vector4 vector) => new Vector4(vector.z, vector.x, vector.y, vector.x);
        public static Vector3 zxyy(this Vector4 vector) => new Vector4(vector.z, vector.x, vector.y, vector.y);
        public static Vector3 zxyz(this Vector4 vector) => new Vector4(vector.z, vector.x, vector.y, vector.z);
        public static Vector3 zxyw(this Vector4 vector) => new Vector4(vector.z, vector.x, vector.y, vector.w);
        public static Vector3 zxzx(this Vector4 vector) => new Vector4(vector.z, vector.x, vector.z, vector.x);
        public static Vector3 zxzy(this Vector4 vector) => new Vector4(vector.z, vector.x, vector.z, vector.y);
        public static Vector3 zxzz(this Vector4 vector) => new Vector4(vector.z, vector.x, vector.z, vector.z);
        public static Vector3 zxzw(this Vector4 vector) => new Vector4(vector.z, vector.x, vector.z, vector.w);
        public static Vector3 zyxx(this Vector4 vector) => new Vector4(vector.z, vector.y, vector.x, vector.x);
        public static Vector3 zyxy(this Vector4 vector) => new Vector4(vector.z, vector.y, vector.x, vector.y);
        public static Vector3 zyxz(this Vector4 vector) => new Vector4(vector.z, vector.y, vector.x, vector.z);
        public static Vector3 zyxw(this Vector4 vector) => new Vector4(vector.z, vector.y, vector.x, vector.w);
        public static Vector3 zyyx(this Vector4 vector) => new Vector4(vector.z, vector.y, vector.y, vector.x);
        public static Vector3 zyyy(this Vector4 vector) => new Vector4(vector.z, vector.y, vector.y, vector.y);
        public static Vector3 zyyz(this Vector4 vector) => new Vector4(vector.z, vector.y, vector.y, vector.z);
        public static Vector3 zyyw(this Vector4 vector) => new Vector4(vector.z, vector.y, vector.y, vector.w);
        public static Vector3 zzxx(this Vector4 vector) => new Vector4(vector.z, vector.z, vector.x, vector.x);
        public static Vector3 zzxy(this Vector4 vector) => new Vector4(vector.z, vector.z, vector.x, vector.y);
        public static Vector3 zzxz(this Vector4 vector) => new Vector4(vector.z, vector.z, vector.x, vector.z);
        public static Vector3 zzxw(this Vector4 vector) => new Vector4(vector.z, vector.z, vector.x, vector.w);
        public static Vector3 zzyx(this Vector4 vector) => new Vector4(vector.z, vector.z, vector.y, vector.x);
        public static Vector3 zzyy(this Vector4 vector) => new Vector4(vector.z, vector.z, vector.y, vector.y);
        public static Vector3 zzyz(this Vector4 vector) => new Vector4(vector.z, vector.z, vector.y, vector.z);
        public static Vector3 zzyw(this Vector4 vector) => new Vector4(vector.z, vector.z, vector.y, vector.w);
        public static Vector3 zzzx(this Vector4 vector) => new Vector4(vector.z, vector.z, vector.z, vector.x);
        public static Vector3 zzzy(this Vector4 vector) => new Vector4(vector.z, vector.z, vector.z, vector.y);
        public static Vector3 zzzz(this Vector4 vector) => new Vector4(vector.z, vector.z, vector.z, vector.z);
        public static Vector3 zzzw(this Vector4 vector) => new Vector4(vector.z, vector.z, vector.z, vector.w);
        public static Vector3 zwxx(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.x, vector.x);
        public static Vector3 zwxy(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.x, vector.y);
        public static Vector3 zwxz(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.x, vector.z);
        public static Vector3 zwxw(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.x, vector.w);
        public static Vector3 zwyx(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.y, vector.x);
        public static Vector3 zwyy(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.y, vector.y);
        public static Vector3 zwyz(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.y, vector.z);
        public static Vector3 zwyw(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.y, vector.w);
        public static Vector3 zwzx(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.z, vector.x);
        public static Vector3 zwzy(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.z, vector.y);
        public static Vector3 zwzz(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.z, vector.z);
        public static Vector3 zwzw(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.z, vector.w);
        public static Vector3 zwwx(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.w, vector.x);
        public static Vector3 zwwy(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.w, vector.y);
        public static Vector3 zwwz(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.w, vector.z);
        public static Vector3 zwww(this Vector4 vector) => new Vector4(vector.z, vector.w, vector.w, vector.w);
        public static Vector3 wxxx(this Vector4 vector) => new Vector4(vector.w, vector.x, vector.x, vector.x);
        public static Vector3 wxxy(this Vector4 vector) => new Vector4(vector.w, vector.x, vector.x, vector.y);
        public static Vector3 wxxz(this Vector4 vector) => new Vector4(vector.w, vector.x, vector.x, vector.z);
        public static Vector3 wxxw(this Vector4 vector) => new Vector4(vector.w, vector.x, vector.x, vector.w);
        public static Vector3 wxyx(this Vector4 vector) => new Vector4(vector.w, vector.x, vector.y, vector.x);
        public static Vector3 wxyy(this Vector4 vector) => new Vector4(vector.w, vector.x, vector.y, vector.y);
        public static Vector3 wxyz(this Vector4 vector) => new Vector4(vector.w, vector.x, vector.y, vector.z);
        public static Vector3 wxyw(this Vector4 vector) => new Vector4(vector.w, vector.x, vector.y, vector.w);
        public static Vector3 wxzx(this Vector4 vector) => new Vector4(vector.w, vector.x, vector.z, vector.x);
        public static Vector3 wxzy(this Vector4 vector) => new Vector4(vector.w, vector.x, vector.z, vector.y);
        public static Vector3 wxzz(this Vector4 vector) => new Vector4(vector.w, vector.x, vector.z, vector.z);
        public static Vector3 wxzw(this Vector4 vector) => new Vector4(vector.w, vector.x, vector.z, vector.w);
        public static Vector3 wyxx(this Vector4 vector) => new Vector4(vector.w, vector.y, vector.x, vector.x);
        public static Vector3 wyxy(this Vector4 vector) => new Vector4(vector.w, vector.y, vector.x, vector.y);
        public static Vector3 wyxz(this Vector4 vector) => new Vector4(vector.w, vector.y, vector.x, vector.z);
        public static Vector3 wyxw(this Vector4 vector) => new Vector4(vector.w, vector.y, vector.x, vector.w);
        public static Vector3 wyyx(this Vector4 vector) => new Vector4(vector.w, vector.y, vector.y, vector.x);
        public static Vector3 wyyy(this Vector4 vector) => new Vector4(vector.w, vector.y, vector.y, vector.y);
        public static Vector3 wyyz(this Vector4 vector) => new Vector4(vector.w, vector.y, vector.y, vector.z);
        public static Vector3 wyyw(this Vector4 vector) => new Vector4(vector.w, vector.y, vector.y, vector.w);
        public static Vector3 wzxx(this Vector4 vector) => new Vector4(vector.w, vector.z, vector.x, vector.x);
        public static Vector3 wzxy(this Vector4 vector) => new Vector4(vector.w, vector.z, vector.x, vector.y);
        public static Vector3 wzxz(this Vector4 vector) => new Vector4(vector.w, vector.z, vector.x, vector.z);
        public static Vector3 wzxw(this Vector4 vector) => new Vector4(vector.w, vector.z, vector.x, vector.w);
        public static Vector3 wzyx(this Vector4 vector) => new Vector4(vector.w, vector.z, vector.y, vector.x);
        public static Vector3 wzyy(this Vector4 vector) => new Vector4(vector.w, vector.z, vector.y, vector.y);
        public static Vector3 wzyz(this Vector4 vector) => new Vector4(vector.w, vector.z, vector.y, vector.z);
        public static Vector3 wzyw(this Vector4 vector) => new Vector4(vector.w, vector.z, vector.y, vector.w);
        public static Vector3 wzzx(this Vector4 vector) => new Vector4(vector.w, vector.z, vector.z, vector.x);
        public static Vector3 wzzy(this Vector4 vector) => new Vector4(vector.w, vector.z, vector.z, vector.y);
        public static Vector3 wzzz(this Vector4 vector) => new Vector4(vector.w, vector.z, vector.z, vector.z);
        public static Vector3 wzzw(this Vector4 vector) => new Vector4(vector.w, vector.z, vector.z, vector.w);
        public static Vector3 wwxx(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.x, vector.x);
        public static Vector3 wwxy(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.x, vector.y);
        public static Vector3 wwxz(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.x, vector.z);
        public static Vector3 wwxw(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.x, vector.w);
        public static Vector3 wwyx(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.y, vector.x);
        public static Vector3 wwyy(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.y, vector.y);
        public static Vector3 wwyz(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.y, vector.z);
        public static Vector3 wwyw(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.y, vector.w);
        public static Vector3 wwzx(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.z, vector.x);
        public static Vector3 wwzy(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.z, vector.y);
        public static Vector3 wwzz(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.z, vector.z);
        public static Vector3 wwzw(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.z, vector.w);
        public static Vector3 wwwx(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.w, vector.x);
        public static Vector3 wwwy(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.w, vector.y);
        public static Vector3 wwwz(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.w, vector.z);
        public static Vector3 wwww(this Vector4 vector) => new Vector4(vector.w, vector.w, vector.w, vector.w);

        #endregion Conversions

    }
}